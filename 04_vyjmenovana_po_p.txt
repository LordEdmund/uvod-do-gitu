Máňa je kopírka v typickém malém hotelu.
Ano, Máňa je pitomé jméno pro stroj, který pilně dělá kopie různých papírů, stránek časopisů, učebnic dějepisu, not na piano či třpytivých letáků. 
Ale jméno si pochopitelně nevybereme.
Dneska těsně před Máňou klopýtnul nezkušený pikolík, takové mladé kopyto, nespisovně zanadával a hupity hup - spadl na Máňu tak stupidně, že se rozcapil na kopírovací plochu a zmáčkl přitom tlačítko.
Máňa se zapýřila a pyšně mu několikrát nakopírovala zadek a tím se zbavila svého jména.
Všichni jí teď popichují oslovením Zadkopírka.
